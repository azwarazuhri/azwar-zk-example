package vmd;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;


import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Include;

import dto.KaryawanDto;
import dto.MstKotaDto;
import dto.MstProvinsiDto;

import entity.Karyawan;
import entity.MstKota;
import entity.MstProvinsi;

import pagevmd.NavigationVmd;
import service.KaryawanSvc;
import service.MstKotaSvc;
import service.MstProvinsiSvc;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017 
 * 
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class KaryawanEditVmd extends NavigationVmd{
	
	//Ini Untuk meload data dari customer
	private List<Karyawan> listKaryawan = new ArrayList<>();
	private KaryawanDto karyawanDto = new KaryawanDto();
	private List<KaryawanDto> karyawanDtos = new ArrayList<>();
	
	//Ini untuk meload data kota dan provinsi
	private List<MstKotaDto> listKota = new ArrayList<>();
	private List<MstKota> listKot = new ArrayList<>();
	private List<MstProvinsiDto> listProv = new ArrayList<>();
	private MstProvinsiDto mstProvinsiDto = new MstProvinsiDto();
	private MstKotaDto mstKotaDto = new MstKotaDto();
	private MstKota mstKota = new MstKota();
	private MstProvinsi mstProvinsi = new MstProvinsi();
	
	
	
	
	@WireVariable
	private KaryawanSvc karyawanSvc;
	
	@WireVariable //ini untuk service kota
	private MstKotaSvc mstKotaSvc;

	@WireVariable // ini untuk service provinsi
	private MstProvinsiSvc mstProvinsiSvc;
	
	@Init // ini unutk meload data apabila tidak ada data karyawan yang terload makan akan insert
	public void load(){
		karyawanDto=(KaryawanDto) Sessions.getCurrent().getAttribute("obj");
		//Messagebox.show("NIK : "+karyawanDto.getNik());
		if (karyawanDto.getNik() != null) {
			listProv = mstProvinsiSvc.findAll();
			listKota = mstKotaSvc.findAllData();
			mstKotaDto = mstKotaSvc.findOne(karyawanDto.getKota());
			MstProvinsiDto aa = mstProvinsiSvc.findOne(mstKotaDto
					.getProvinsiId());
			String bb = aa.getProvinsiId();
			if (aa != null) {
				mstProvinsiDto.setProvinsiId(bb);
				mstProvinsiDto.setNamaProvinsi(aa.getNamaProvinsi());
			}
			findkotaEdit();
		} else {
			listKota = mstKotaSvc.findAllData();
			listProv = mstProvinsiSvc.findAll();
		}
	}
	
	@Command("save") // method untuk edit dan simpan apabila data tersebut tidak ada
	public void save(){
		karyawanSvc.save(karyawanDto);
		Clients.showNotification("Data berhasil disimpan", Clients.NOTIFICATION_TYPE_INFO, null, null,1500);
	}
	
	@Command("back") // method untuk kembali ke menu utama
	public void back() {
		 Include inc = (Include)
		 Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
		 inc.setSrc("index.zul");
		//Executions.sendRedirect("/master/customer/customer.zul");
	}
	
	@Command("findkota") // method unutuk mencari kota
	@NotifyChange({ "listKota", "mstKotaDto" })
	public void findKota() {
		mstKotaDto = null;
		String kode = mstProvinsiDto.getProvinsiId();
		System.out.println(kode);
		listKota = mstKotaSvc.findKota(kode);

	}

	@Command("findkotaEdit") // method untuk edit kota
	@NotifyChange({ "listKota", "mstKotaDto" })
	public void findkotaEdit() {
		String kode = mstProvinsiDto.getProvinsiId();
		listKota = mstKotaSvc.findKota(kode);

	}

	
	
	
	
	
	
	
	
	
	
	
	public List<MstKotaDto> getListKota() {
		return listKota;
	}



	public void setListKota(List<MstKotaDto> listKota) {
		this.listKota = listKota;
	}



	public List<MstKota> getListKot() {
		return listKot;
	}



	public void setListKot(List<MstKota> listKot) {
		this.listKot = listKot;
	}



	public List<MstProvinsiDto> getListProv() {
		return listProv;
	}



	public void setListProv(List<MstProvinsiDto> listProv) {
		this.listProv = listProv;
	}



	public MstProvinsiDto getMstProvinsiDto() {
		return mstProvinsiDto;
	}



	public void setMstProvinsiDto(MstProvinsiDto mstProvinsiDto) {
		this.mstProvinsiDto = mstProvinsiDto;
	}



	public MstKotaDto getMstKotaDto() {
		return mstKotaDto;
	}



	public void setMstKotaDto(MstKotaDto mstKotaDto) {
		this.mstKotaDto = mstKotaDto;
	}



	public MstKota getMstKota() {
		return mstKota;
	}



	public void setMstKota(MstKota mstKota) {
		this.mstKota = mstKota;
	}



	public MstProvinsi getMstProvinsi() {
		return mstProvinsi;
	}



	public void setMstProvinsi(MstProvinsi mstProvinsi) {
		this.mstProvinsi = mstProvinsi;
	}

	
	
	

	public KaryawanDto getKaryawanDto() {
		return karyawanDto;
	}



	public void setKaryawanDto(KaryawanDto karyawanDto) {
		this.karyawanDto = karyawanDto;
	}



	public List<Karyawan> getListKaryawan() {
		return listKaryawan;
	}

	public void setListKaryawan(List<Karyawan> listKaryawan) {
		this.listKaryawan = listKaryawan;
	}

	public List<KaryawanDto> getKaryawanDtos() {
		return karyawanDtos;
	}

	public void setKaryawanDtos(List<KaryawanDto> karyawanDtos) {
		this.karyawanDtos = karyawanDtos;
	}

	
}
