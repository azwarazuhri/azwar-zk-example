package vmd;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zul.Include;


import org.zkoss.zul.Messagebox;

import dto.KaryawanDto;
import entity.Karyawan;
import pagevmd.NavigationVmd;
import service.KaryawanSvc;
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class KaryawanVmd extends NavigationVmd {
	
	
	//--------- ini logic untuk ngeload data Karyawan Yang nomer Satu ----------------- //
	private List<Karyawan>listKaryawan = new ArrayList<>();
	private KaryawanDto karyawanDto = new KaryawanDto();
	private List<KaryawanDto> karyawanDtos = new ArrayList<>();
	
	
	@WireVariable // ini variable yang dari servicenya yang bakal di panggil zul as service
	private KaryawanSvc karyawanSvc;
	
	
	@Init //-> ini varibale yang abakal di panggil  untuk meload semua data
	public void load(){
		setKaryawanDtos(karyawanSvc.findAll());
	}
	
	
	@Command("add") // -> ini method untuk ke ui add data
	public void add(){
		karyawanDto = new KaryawanDto();
		Sessions.getCurrent().setAttribute("obj", karyawanDto);
		Include inc = (Include) Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
		inc.setSrc("/ujian/new_file.zul");
	}
	
	
	@Command("edit") // -> ini method untuk ke ui edit data by selected data
	public void edit(){
		if(karyawanDto.getNik()==null){
			Messagebox.show("Pilih dulu datanya yang mau di pilih mas bro !");
		}else{
			Sessions.getCurrent().setAttribute("obj", karyawanDto);
			Include inc = (Include) Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
			inc.setSrc("/ujian/new_file.zul");
		}
	}
	
	

	//--------------[START] Setter getternya karyawan ---------//
	public List<Karyawan> getListKaryawan() {
		return listKaryawan;
	}

	public void setListKaryawan(List<Karyawan> listKaryawan) {
		this.listKaryawan = listKaryawan;
	}
	
	public KaryawanDto getKaryawanDto() {
		return karyawanDto;
	}

	public void setKaryawanDto(KaryawanDto karyawanDto) {
		this.karyawanDto = karyawanDto;
	}

	public List<KaryawanDto> getKaryawanDtos() {
		return karyawanDtos;
	}

	public void setKaryawanDtos(List<KaryawanDto> karyawanDtos) {
		this.karyawanDtos = karyawanDtos;
	}
	//--------------[END] Setter getternya karyawan ---------//

	
	
}
