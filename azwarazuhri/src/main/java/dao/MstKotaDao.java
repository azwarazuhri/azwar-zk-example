package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import dto.MstKotaDto;
import entity.MstKota;
import entity.MstKotaPk;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public interface MstKotaDao extends JpaRepository<MstKota, MstKotaPk> {
	
	@Query(" select a, b.namaProvinsi from MstKota a, MstProvinsi b "
			+ " where a.provinsiId = b.provinsiId ")
	public List<Object[]> findAllKota();
	
	@Query(" select a from MstKota a "
			+ " where a.kotaId = :kotaId ")
	public MstKota findOneKota (@Param("kotaId")String kotaId);
	
	

	@Query(" select a from MstKota a ")
	public List<MstKota> findAll ();
	
	
	@Query(" select a from MstKota a "
			+ " where a.provinsiId = :provinsiId ")
	public List<MstKota> findOneKotaByProv (@Param("provinsiId")String provinsiId);

}
