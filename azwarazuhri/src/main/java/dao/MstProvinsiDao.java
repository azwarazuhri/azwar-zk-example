package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstProvinsi;
import entity.MstProvinsiPk;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public interface MstProvinsiDao extends JpaRepository<MstProvinsi, MstProvinsiPk> {
	
	@Query(" select a from MstProvinsi a ")
	public List<MstProvinsi> findAllProvinsi();
	
	@Query(" select a from MstProvinsi a "
			+ " where a.provinsiId = :provinsiId ")
	public MstProvinsi findOneProvinsi (@Param("provinsiId") String provinsiId);

}
