package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import entity.Karyawan;
import entity.KaryawanPK;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */

public interface KaryawanDao extends JpaRepository<Karyawan, KaryawanPK>{
	@Query("SELECT k from Karyawan k")
	public List<Karyawan> findAllKaryawan();
}
