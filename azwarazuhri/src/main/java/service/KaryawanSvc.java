package service;

import java.util.List;

import dto.KaryawanDto;

/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public interface KaryawanSvc {
	public List<KaryawanDto> findAll();
	public void save(KaryawanDto karyawanDto);
	public void delete(String karyawanDto);
}
