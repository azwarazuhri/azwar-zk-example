package service;

import java.util.List;

import dto.MstProvinsiDto;
import entity.MstProvinsi;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public interface MstProvinsiSvc {
	public List<MstProvinsiDto> findAll();
	public void save(MstProvinsiDto dto);
	public void update(MstProvinsiDto dto);
	public void delete(String provinsiId);
	public MstProvinsiDto findOne(String provinsiId);
}
