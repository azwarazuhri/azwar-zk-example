package service;

import java.util.List;

import dto.MstKotaDto;
import entity.MstKota;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public interface MstKotaSvc {
	public List<MstKotaDto> findAllData();
	public void save (MstKotaDto mstKotaDto);
	public void update(MstKotaDto mstKotaDto);
	public void delete (String kotaId);
	public MstKotaDto findOne(String kotaId);
	public List<MstKotaDto> findKota(String provinsiId);
//	public List<MstKotaDto> findAll ();

}
