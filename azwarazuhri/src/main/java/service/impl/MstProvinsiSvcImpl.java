package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MstProvinsiDao;
import dto.MstProvinsiDto;
import entity.MstProvinsi;
import entity.MstProvinsiPk;
import service.MstProvinsiSvc;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
@Service("mstProvinsiSvc")
@Transactional
public class MstProvinsiSvcImpl implements MstProvinsiSvc{

	@Autowired
	private MstProvinsiDao mstProvinsiDao;
	
	
	@Override
	public List<MstProvinsiDto> findAll() {
		List<MstProvinsi> listObject = mstProvinsiDao.findAllProvinsi();
		List<MstProvinsiDto> mstProvinsiDtos = new ArrayList<>();
		
		for(MstProvinsi obj : listObject){
		MstProvinsiDto mstProvinsiDto = new MstProvinsiDto();
//		MstProvinsi mstProvinsi =new MstProvinsi();
		mstProvinsiDto.setProvinsiId(obj.getProvinsiId());
		mstProvinsiDto.setNamaProvinsi(obj.getNamaProvinsi());
		//obj.getNamaProvinsi();
		
		mstProvinsiDtos.add(mstProvinsiDto);
		}
		return mstProvinsiDtos;
	}

	@Override
	public void save(MstProvinsiDto dto) {
		// TODO Auto-generated method stub
		MstProvinsi  mstCustomer = new MstProvinsi();
		mstCustomer.setProvinsiId(dto.getProvinsiId());
		mstCustomer.setNamaProvinsi(dto.getNamaProvinsi());
		mstProvinsiDao.save(mstCustomer);
	}

	@Override
	public void update(MstProvinsiDto dto) {
		// TODO Auto-generated method stub
		MstProvinsi  mstCustomer = new MstProvinsi();
		mstCustomer.setProvinsiId(dto.getProvinsiId());
		mstCustomer.setNamaProvinsi(dto.getNamaProvinsi());
		mstProvinsiDao.save(mstCustomer);
	}

	@Override
	public void delete(String provinsiId) {
		// TODO Auto-generated method stub
		MstProvinsiPk mstProvinsiPk = new MstProvinsiPk();
		mstProvinsiPk.setProvinsiId(provinsiId);
		mstProvinsiDao.delete(mstProvinsiPk);
	
	}

	@Override
	public MstProvinsiDto findOne(String provinsiId) {
		MstProvinsi aa = mstProvinsiDao.findOneProvinsi(provinsiId);
		MstProvinsiDto mstProvinsiDto = new MstProvinsiDto();
		
		if(aa != null){
			mstProvinsiDto.setProvinsiId(aa.getProvinsiId());
			mstProvinsiDto.setNamaProvinsi(aa.getNamaProvinsi());
			return mstProvinsiDto;
		}
		return mstProvinsiDto;
	}

}
