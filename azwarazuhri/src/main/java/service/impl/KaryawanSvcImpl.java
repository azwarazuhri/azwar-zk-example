package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.KaryawanDao;
import dto.KaryawanDto;
import entity.Karyawan;
import entity.KaryawanPK;
import service.KaryawanSvc;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
@Service("karyawanSvc")
@Transactional
public class KaryawanSvcImpl implements KaryawanSvc{
	@Autowired
	private KaryawanDao karyawanDao;
	@Override
	public List<KaryawanDto> findAll() {
		List<Karyawan> listObject=karyawanDao.findAllKaryawan();
		List<KaryawanDto> karyawanDtos = new ArrayList<>();
		for(Karyawan obj: listObject){
			KaryawanDto karyawanDto = new KaryawanDto();
			karyawanDto.setNik(obj.getNik());
			karyawanDto.setNama(obj.getNama());
			karyawanDto.setJenisKelamin(obj.getJenisKelamin());
			karyawanDto.setEmail(obj.getEmail());
			karyawanDto.setGaji(obj.getGaji());
			karyawanDto.setProvinsi(obj.getProvinsi());
			karyawanDto.setKota(obj.getKota());
			karyawanDto.setTanggalJoin(obj.getTanggalJoin());
			karyawanDto.setTanggalLahir(obj.getTanggalLahir());
			karyawanDto.setUsia(obj.getUsia());
			karyawanDto.setNamaAyah(obj.getNamaAyah());
			karyawanDto.setTanggalLahirAyah(obj.getTanggalLahirAyah());
			karyawanDto.setHubunganAyah(obj.getHubunganAyah());
			karyawanDto.setPendidikanAyah(obj.getPendidikanAyah());
			karyawanDto.setNamaIbu(obj.getNamaIbu());
			karyawanDto.setTanggalLahirIbu(obj.getTanggalLahirIbu());
			karyawanDto.setHubunganIbu(obj.getHubunganIbu());
			karyawanDto.setPendidikanIbu(obj.getPendidikanIbu());
			karyawanDtos.add(karyawanDto);
		}
		return karyawanDtos;
	}
	@Override
	public void save(KaryawanDto karyawanDto) {
		// TODO Auto-generated method stub
		Karyawan karyawan = new Karyawan();
		karyawan.setNik(karyawanDto.getNik());
		karyawan.setNama(karyawanDto.getNama());
		karyawan.setJenisKelamin(karyawanDto.getJenisKelamin());
		karyawan.setEmail(karyawanDto.getEmail());
		karyawan.setGaji(karyawanDto.getGaji());
		karyawan.setProvinsi(karyawanDto.getProvinsi());
		karyawan.setKota(karyawanDto.getKota());
		karyawan.setTanggalJoin(karyawanDto.getTanggalJoin());
		karyawan.setTanggalLahir(karyawanDto.getTanggalLahir());
		karyawan.setUsia(karyawanDto.getUsia());
		karyawan.setNamaAyah(karyawanDto.getNamaAyah());
		karyawan.setTanggalLahirAyah(karyawanDto.getTanggalLahirAyah());
		karyawan.setHubunganAyah(karyawanDto.getHubunganAyah());
		karyawan.setPendidikanAyah(karyawanDto.getPendidikanAyah());
		karyawan.setNamaIbu(karyawanDto.getNamaIbu());
		karyawan.setTanggalLahirIbu(karyawanDto.getTanggalLahirIbu());
		karyawan.setHubunganIbu(karyawanDto.getHubunganIbu());
		karyawan.setPendidikanIbu(karyawanDto.getPendidikanIbu());
		karyawanDao.save(karyawan);
	}
	@Override
	public void delete(String karyawanDto) {
		// TODO Auto-generated method stub
		KaryawanPK karyawanPK = new KaryawanPK();
		karyawanPK.setNik(karyawanDto);
		karyawanDao.delete(karyawanPK);
	}

}
