package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MstKotaDao;
import dto.MstKotaDto;
import entity.MstKota;
import entity.MstKotaPk;
import service.MstKotaSvc;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */

@Service("mstKotaSvc")
@Transactional
public class MstKotaSvcImpl implements MstKotaSvc {

	
	@Autowired
	private MstKotaDao mstKotaDao;
	
	@Override
	public List<MstKotaDto> findAllData() {
		List<Object[]> listObject = mstKotaDao.findAllKota();
		List<MstKotaDto> mstKotaDtos = new ArrayList<>();
		
		for(Object [] obj : listObject){
		MstKotaDto mstKotaDto = new MstKotaDto();
		MstKota aa = (MstKota)obj[0];
		mstKotaDto.setKotaId(aa.getKotaId());
		mstKotaDto.setNamaKota(aa.getNamaKota());
		mstKotaDto.setNamaProvinsi((String) obj[1]);
		mstKotaDtos.add(mstKotaDto);
		}
		return mstKotaDtos;
	}

	@Override
	public void save(MstKotaDto mstKotaDto) {
		// TODO Auto-generated method stub
		MstKota mstKota = new MstKota();
		mstKota.setKotaId(mstKotaDto.getKotaId());
		mstKota.setNamaKota(mstKotaDto.getNamaKota());
		mstKota.setProvinsiId(mstKotaDto.getProvinsiId());
		mstKotaDao.save(mstKota);
	}

	@Override
	public void update(MstKotaDto mstKotaDto) {
		// TODO Auto-generated method stub
		MstKota mstKota = new MstKota();
		mstKota.setKotaId(mstKotaDto.getKotaId());
		mstKota.setNamaKota(mstKotaDto.getNamaKota());
		mstKota.setProvinsiId(mstKotaDto.getProvinsiId());
		mstKotaDao.save(mstKota);
	}

	@Override
	public void delete(String kotaId) {
		// TODO Auto-generated method stub
		MstKotaPk mstKotaPk = new MstKotaPk();
		mstKotaPk.setKotaId(kotaId);
		mstKotaDao.delete(mstKotaPk);
	}

	@Override
	public MstKotaDto findOne(String kotaId) {
		// TODO Auto-generated method stub
		MstKota aa = mstKotaDao.findOneKota(kotaId);
		MstKotaDto mstKotaDto = new MstKotaDto();
		if(aa != null){
			mstKotaDto.setKotaId(aa.getKotaId());
			mstKotaDto.setNamaKota(aa.getNamaKota());
			mstKotaDto.setProvinsiId(aa.getProvinsiId());
			return mstKotaDto;		
			}
		return mstKotaDto;		
			
	}

	@Override
	public List<MstKotaDto> findKota(String provinsiId) {
		// TODO Auto-generated method stub
		List<MstKota> listObject = mstKotaDao.findOneKotaByProv(provinsiId);
		List<MstKotaDto> mstKota = new ArrayList<>();
		
		for(MstKota aa : listObject){
		MstKotaDto mstKota2 = new MstKotaDto();
		
		mstKota2.setKotaId(aa.getKotaId());
		mstKota2.setNamaKota(aa.getNamaKota());
		mstKota2.setProvinsiId(aa.getProvinsiId());
		mstKota.add(mstKota2);
		}
		return mstKota;
	}

//	@Override
//	public List<MstKota> findAll() {
//		List<MstKota> listObject = mstKotaDao.findAll();
//		List<MstKota> mstKota = new ArrayList<>();
//		
//		for(MstKota aa : listObject){
//		MstKota mstKota2 = new MstKota();
//		
//		mstKota2.setKotaId(aa.getKotaId());
//		mstKota2.setNamaKota(aa.getNamaKota());
//		mstKota2.setProvinsiId(aa.getProvinsiId());
//		mstKota.add(mstKota2);
//		}
//		return mstKota;
//
//	}

}
