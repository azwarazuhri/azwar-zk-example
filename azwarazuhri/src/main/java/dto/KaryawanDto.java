package dto;

import java.sql.Date;

/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public class KaryawanDto {
	private String nik;
	private String nama;
	private String jenisKelamin;
	private String email;
	private int gaji;
	private String provinsi;
	private String kota;
	private Date tanggalJoin;
	private Date tanggalLahir;
	private int usia;
	private String namaAyah;
	private Date tanggalLahirAyah;
	private String hubunganAyah;
	private String pendidikanAyah;
	private String namaIbu;
	private Date tanggalLahirIbu;
	private String hubunganIbu;
	private String pendidikanIbu;
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getGaji() {
		return gaji;
	}
	public void setGaji(int gaji) {
		this.gaji = gaji;
	}
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	
	public int getUsia() {
		return usia;
	}
	public void setUsia(int usia) {
		this.usia = usia;
	}
	public String getNamaAyah() {
		return namaAyah;
	}
	public void setNamaAyah(String namaAyah) {
		this.namaAyah = namaAyah;
	}
	
	public String getPendidikanAyah() {
		return pendidikanAyah;
	}
	public void setPendidikanAyah(String pendidikanAyah) {
		this.pendidikanAyah = pendidikanAyah;
	}
	public String getNamaIbu() {
		return namaIbu;
	}
	public void setNamaIbu(String namaIbu) {
		this.namaIbu = namaIbu;
	}
	
	public String getHubunganIbu() {
		return hubunganIbu;
	}
	public void setHubunganIbu(String hubunganIbu) {
		this.hubunganIbu = hubunganIbu;
	}
	public String getPendidikanIbu() {
		return pendidikanIbu;
	}
	public void setPendidikanIbu(String pendidikanIbu) {
		this.pendidikanIbu = pendidikanIbu;
	}
	public String getHubunganAyah() {
		return hubunganAyah;
	}
	public void setHubunganAyah(String hubunganAyah) {
		this.hubunganAyah = hubunganAyah;
	}
	public Date getTanggalJoin() {
		return tanggalJoin;
	}
	public void setTanggalJoin(Date tanggalJoin) {
		this.tanggalJoin = tanggalJoin;
	}
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public Date getTanggalLahirAyah() {
		return tanggalLahirAyah;
	}
	public void setTanggalLahirAyah(Date tanggalLahirAyah) {
		this.tanggalLahirAyah = tanggalLahirAyah;
	}
	public Date getTanggalLahirIbu() {
		return tanggalLahirIbu;
	}
	public void setTanggalLahirIbu(Date tanggalLahirIbu) {
		this.tanggalLahirIbu = tanggalLahirIbu;
	}
	
	
}
