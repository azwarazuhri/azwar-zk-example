package dto;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public class MstProvinsiDto {
	private String provinsiId;
	private String namaProvinsi;
	
	

	public String getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}
	

	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
}
