package dto;

/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */


public class MstKotaDto {
	private String kotaId;
	private String namaKota;
	private String provinsiId;
	private String namaProvinsi;
	

	public String getNamaProvinsi() {
		return namaProvinsi;
	}


	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
	public String getKotaId() {
		return kotaId;
	}
	public void setKotaId(String kotaId) {
		this.kotaId = kotaId;
	}
	

	public String getNamaKota() {
		return namaKota;
	}
	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}
	

	public String getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}

	
}
