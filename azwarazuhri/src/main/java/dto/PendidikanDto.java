package dto;

/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public class PendidikanDto {
	private String kodePendidikan;
	private String namaPendidikan;
	
	
	public String getKodePendidikan() {
		return kodePendidikan;
	}
	public void setKodePendidikan(String kodePendidikan) {
		this.kodePendidikan = kodePendidikan;
	}
	
	
	public String getNamaPendidikan() {
		return namaPendidikan;
	}
	public void setNamaPendidikan(String namaPendidikan) {
		this.namaPendidikan = namaPendidikan;
	}
}
