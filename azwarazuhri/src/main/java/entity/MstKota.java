package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;



@Entity
@Table(name="MST_KOTA")
@IdClass(MstKotaPk.class)
public class MstKota {
	private String kotaId;
	private String namaKota;
	private String provinsiId;
	
	
	
	@Id
	@Column(name="KODE_KOTA")
	public String getKotaId() {
		return kotaId;
	}
	public void setKotaId(String kotaId) {
		this.kotaId = kotaId;
	}
	
	@Column(name="NAMA_KOTA")
	public String getNamaKota() {
		return namaKota;
	}
	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}
	
	@Column(name="KODE_PROVINSI")
	public String getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}

	
}
