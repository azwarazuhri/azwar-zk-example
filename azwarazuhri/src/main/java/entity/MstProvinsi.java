package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;



@Entity
@Table(name="MST_PROVINSI")
@IdClass(MstProvinsiPk.class)
public class MstProvinsi {
	private String provinsiId;
	private String namaProvinsi;
	
	
	@Id
	@Column(name="KODE_PROVINSI")
	public String getProvinsiId() {
		return provinsiId;
	}
	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}
	
	@Column(name="NAMA_PROVINSI")
	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
}
