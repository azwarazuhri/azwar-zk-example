package entity;

import java.io.Serializable;

public class MstProvinsiPk implements Serializable {
	private static final long serialVersionUID = 1L;
	private String provinsiId;
	
	public MstProvinsiPk(){}

	public String getProvinsiId() {
		return provinsiId;
	}

	public void setProvinsiId(String provinsiId) {
		this.provinsiId = provinsiId;
	}
	
	
	
}
