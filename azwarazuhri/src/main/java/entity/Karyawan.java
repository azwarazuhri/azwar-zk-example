package entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
@Entity
@Table(name="KaryawanDB2")
@IdClass(KaryawanPK.class)
public class Karyawan {
	private String nik;
	private String nama;
	private String jenisKelamin;
	private String email;
	private int gaji;
	private String provinsi;
	private String kota;
	private Date tanggalJoin;
	private Date tanggalLahir;
	private int usia;
	private String namaAyah;
	private Date tanggalLahirAyah;
	private String hubunganAyah;
	private String pendidikanAyah;
	private String namaIbu;
	private Date tanggalLahirIbu;
	private String hubunganIbu;
	private String pendidikanIbu;
	
	@Id
	@Column(name="nik")
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	
	@Column(name="nama")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="jenisKelamin")
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="gaji")
	public int getGaji() {
		return gaji;
	}
	public void setGaji(int gaji) {
		this.gaji = gaji;
	}
	
	@Column(name="provinsi")
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	
	@Column(name="kota")
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	
	@Column(name="usia")
	public int getUsia() {
		return usia;
	}
	public void setUsia(int usia) {
		this.usia = usia;
	}
	
	@Column(name="namaAyah")
	public String getNamaAyah() {
		return namaAyah;
	}
	public void setNamaAyah(String namaAyah) {
		this.namaAyah = namaAyah;
	}
	
	
	
	
	@Column(name="pendidikanAyah")
	public String getPendidikanAyah() {
		return pendidikanAyah;
	}
	public void setPendidikanAyah(String pendidikanAyah) {
		this.pendidikanAyah = pendidikanAyah;
	}
	
	@Column(name="namaIbu")
	public String getNamaIbu() {
		return namaIbu;
	}
	public void setNamaIbu(String namaIbu) {
		this.namaIbu = namaIbu;
	}
	
	
	
	
	@Column(name="hubunganIbu")
	public String getHubunganIbu() {
		return hubunganIbu;
	}
	public void setHubunganIbu(String hubunganIbu) {
		this.hubunganIbu = hubunganIbu;
	}
	
	@Column(name="pendidikanIbu")
	public String getPendidikanIbu() {
		return pendidikanIbu;
	}
	public void setPendidikanIbu(String pendidikanIbu) {
		this.pendidikanIbu = pendidikanIbu;
	}
	
	@Column(name="hubunganAyah")
	public String getHubunganAyah() {
		return hubunganAyah;
	}
	public void setHubunganAyah(String hubunganAyah) {
		this.hubunganAyah = hubunganAyah;
	}
	
	@Column(name="tanggalJoin")
	public Date getTanggalJoin() {
		return tanggalJoin;
	}
	public void setTanggalJoin(Date tanggalJoin) {
		this.tanggalJoin = tanggalJoin;
	}
	
	@Column(name="tanggalLahir")
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	
	@Column(name="tanggalLahirAyah")
	public Date getTanggalLahirAyah() {
		return tanggalLahirAyah;
	}
	public void setTanggalLahirAyah(Date tanggalLahirAyah) {
		this.tanggalLahirAyah = tanggalLahirAyah;
	}
	
	@Column(name="tanggalLahirIbu")
	public Date getTanggalLahirIbu() {
		return tanggalLahirIbu;
	}
	public void setTanggalLahirIbu(Date tanggalLahirIbu) {
		this.tanggalLahirIbu = tanggalLahirIbu;
	}
	
	
}
