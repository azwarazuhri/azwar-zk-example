package entity;

import java.io.Serializable;

public class MstKotaPk implements Serializable{
	private static final long serialVersionUID = 1L;
	private String kotaId;
	
	public MstKotaPk(){}

	public String getKotaId() {
		return kotaId;
	}

	public void setKotaId(String kotaId) {
		this.kotaId = kotaId;
	}
	
	
	
}
