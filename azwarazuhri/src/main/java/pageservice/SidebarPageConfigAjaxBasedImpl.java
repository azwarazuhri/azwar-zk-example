package pageservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class SidebarPageConfigAjaxBasedImpl implements SidebarPageConfig {

	HashMap<String, SidebarPage> pageMap = new LinkedHashMap<String, SidebarPage>();
	public SidebarPageConfigAjaxBasedImpl(){
		pageMap.put("fn1",new SidebarPage("Ujian Java with ZK Framework","/imgs/fn.png","/ujian/soalsatu.zul"));
			
	}
	@Override
	public List<SidebarPage> getPages() {
		// TODO Auto-generated method stub
		return new ArrayList<SidebarPage>(pageMap.values());
	}
	
}
