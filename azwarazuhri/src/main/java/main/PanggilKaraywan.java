package main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.KaryawanDao;
import entity.Karyawan;

/**
 * DONT REMOVE THIS MARK OR I'LL KICK YOUR ASS
 * Copyright : Azwar Annas Azuhri @Indocyber 2017
 * for more source code visit my github : github.com/azwarfx
 */
public class PanggilKaraywan {
	public static void main(String [] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/META-INF/spring/app-config.xml");
		KaryawanDao kardao = ctx.getBean(KaryawanDao.class);
		
		List<Karyawan> listkar = kardao.findAllKaryawan();
		for(Karyawan k :listkar){
			System.out.println(k.getNik());
		}
	}
}
