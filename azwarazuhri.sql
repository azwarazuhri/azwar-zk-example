USE [master]
GO
/****** Object:  Database [UjianZKAzwarAzuhri]    Script Date: 31/08/2017 17:51:28 ******/
CREATE DATABASE [UjianZKAzwarAzuhri]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UjianZKAzwarAzuhri', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UjianZKAzwarAzuhri.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UjianZKAzwarAzuhri_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UjianZKAzwarAzuhri_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UjianZKAzwarAzuhri].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ARITHABORT OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET  MULTI_USER 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UjianZKAzwarAzuhri]
GO
/****** Object:  Table [dbo].[Hubungan]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hubungan](
	[KODE_HUBUNGAN] [varchar](150) NOT NULL,
	[NAMA_HUBUNGAN] [varchar](150) NULL,
 CONSTRAINT [PK_Hubungan] PRIMARY KEY CLUSTERED 
(
	[KODE_HUBUNGAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KaryawanDB]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KaryawanDB](
	[nik] [varchar](50) NULL,
	[nama] [varchar](150) NULL,
	[jenisKelamin] [varchar](50) NULL,
	[email] [varchar](80) NULL,
	[gaji] [int] NULL,
	[provinsi] [varchar](150) NULL,
	[kota] [varchar](150) NULL,
	[tanggalJoin] [date] NULL,
	[tanggalLahir] [date] NULL,
	[usia] [int] NULL,
	[namaFamily] [varchar](150) NULL,
	[hubungan] [varchar](50) NULL,
	[tanggalLahirAyah] [date] NULL,
	[tanggalLahirIbu] [date] NULL,
	[pendidikanIbu] [varchar](50) NULL,
	[pendidikanAyah] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KaryawanDB2]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KaryawanDB2](
	[nik] [varchar](50) NOT NULL,
	[nama] [varchar](150) NULL,
	[jenisKelamin] [varchar](50) NULL,
	[email] [varchar](80) NULL,
	[gaji] [int] NULL,
	[provinsi] [varchar](150) NULL,
	[kota] [varchar](150) NULL,
	[tanggalJoin] [date] NULL,
	[tanggalLahir] [date] NULL,
	[usia] [int] NULL,
	[namaAyah] [varchar](150) NULL,
	[tanggalLahirAyah] [date] NULL,
	[hubunganAyah] [varchar](50) NULL,
	[pendidikanAyah] [varchar](50) NULL,
	[namaIbu] [varchar](150) NULL,
	[tanggalLahirIbu] [date] NULL,
	[hubunganIbu] [varchar](50) NULL,
	[pendidikanIbu] [varchar](50) NULL,
 CONSTRAINT [PK_KaryawanDB2] PRIMARY KEY CLUSTERED 
(
	[nik] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MST_KOTA]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MST_KOTA](
	[KODE_KOTA] [varchar](50) NOT NULL,
	[NAMA_KOTA] [varchar](50) NULL,
	[KODE_PROVINSI] [varchar](50) NULL,
 CONSTRAINT [PK_MST_KOTA] PRIMARY KEY CLUSTERED 
(
	[KODE_KOTA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MST_PROVINSI]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MST_PROVINSI](
	[KODE_PROVINSI] [varchar](50) NOT NULL,
	[NAMA_PROVINSI] [varchar](50) NULL,
 CONSTRAINT [PK_MST_PROVINSI] PRIMARY KEY CLUSTERED 
(
	[KODE_PROVINSI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pendidikan]    Script Date: 31/08/2017 17:51:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pendidikan](
	[KODE_PENDIDKAN] [varchar](150) NOT NULL,
	[NAMA_PENDIDKAN] [varchar](150) NULL,
 CONSTRAINT [PK_Pendidikan] PRIMARY KEY CLUSTERED 
(
	[KODE_PENDIDKAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[KaryawanDB2] ([nik], [nama], [jenisKelamin], [email], [gaji], [provinsi], [kota], [tanggalJoin], [tanggalLahir], [usia], [namaAyah], [tanggalLahirAyah], [hubunganAyah], [pendidikanAyah], [namaIbu], [tanggalLahirIbu], [hubunganIbu], [pendidikanIbu]) VALUES (N'201243500101', N'Azwar Annas', N'Wanita', N'azwarannas_95@yahoo.com', 7000000, N'P001', N'K001', CAST(0x02360B00 AS Date), CAST(0x79190B00 AS Date), 44, N'Abdul Syakur', CAST(0xD1EB0A00 AS Date), N'Ayah Kandung', N'SMEA', N'Maswanih', CAST(0x60F40A00 AS Date), N'Ibu kandung', N'SMP')
INSERT [dbo].[KaryawanDB2] ([nik], [nama], [jenisKelamin], [email], [gaji], [provinsi], [kota], [tanggalJoin], [tanggalLahir], [usia], [namaAyah], [tanggalLahirAyah], [hubunganAyah], [pendidikanAyah], [namaIbu], [tanggalLahirIbu], [hubunganIbu], [pendidikanIbu]) VALUES (N'201243500102', N'Noviana Dewi', N'Wanita', N'novianadewi@gmail.com', 7000000, N'P002', N'K002', CAST(0x22360B00 AS Date), CAST(0x99190B00 AS Date), 22, N'Mumin', CAST(0xD1EB0A00 AS Date), N'Ayah Kandung', N'SMEA', N'Warsini', CAST(0x60F40A00 AS Date), N'Ibu Kandung', N'SMP')
INSERT [dbo].[KaryawanDB2] ([nik], [nama], [jenisKelamin], [email], [gaji], [provinsi], [kota], [tanggalJoin], [tanggalLahir], [usia], [namaAyah], [tanggalLahirAyah], [hubunganAyah], [pendidikanAyah], [namaIbu], [tanggalLahirIbu], [hubunganIbu], [pendidikanIbu]) VALUES (N'tess', NULL, N'Pria', NULL, 500000, NULL, NULL, CAST(0x1D3D0B00 AS Date), CAST(0x1D3D0B00 AS Date), 0, N'ayah', CAST(0x243D0B00 AS Date), NULL, NULL, N'nama', CAST(0x1E3D0B00 AS Date), NULL, NULL)
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K001', N'Jakarta', N'P001')
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K002', N'Malang', N'P002')
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K003', N'Semarang', N'P002')
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K004', N'Bekasi', N'P003')
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K005', N'Bandung', N'P003')
INSERT [dbo].[MST_KOTA] ([KODE_KOTA], [NAMA_KOTA], [KODE_PROVINSI]) VALUES (N'K006', N'Depok', N'P003')
INSERT [dbo].[MST_PROVINSI] ([KODE_PROVINSI], [NAMA_PROVINSI]) VALUES (N'P001', N'DKI JAKARTA')
INSERT [dbo].[MST_PROVINSI] ([KODE_PROVINSI], [NAMA_PROVINSI]) VALUES (N'P002', N'JAWA TENGAH')
INSERT [dbo].[MST_PROVINSI] ([KODE_PROVINSI], [NAMA_PROVINSI]) VALUES (N'P003', N'JAWA BARAT')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'1', N'SD')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'2', N'SMP')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'3', N'SMA')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'4', N'S1')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'5', N'S2')
INSERT [dbo].[Pendidikan] ([KODE_PENDIDKAN], [NAMA_PENDIDKAN]) VALUES (N'6', N'S3')
USE [master]
GO
ALTER DATABASE [UjianZKAzwarAzuhri] SET  READ_WRITE 
GO
